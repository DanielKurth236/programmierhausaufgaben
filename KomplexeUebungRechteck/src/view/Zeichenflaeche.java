package view;

import java.awt.Graphics;
import java.util.List;

import javax.swing.JPanel;
import controller.BunteRechteckeController;
import model.Rechteck;

@SuppressWarnings("serial")
public class Zeichenflaeche extends JPanel {
	
	private BunteRechteckeController brc;
	
	public Zeichenflaeche(BunteRechteckeController brc) {
		this.brc = brc;
	}
	@Override
	public void paintComponent(Graphics g) {
		List<Rechteck> rechtecke = brc.getRechtecke();
		for(int i = 0; i < rechtecke.size();i++) {
			g.drawRect(rechtecke.get(i).getX(), rechtecke.get(i).getY(), rechtecke.get(i).getBreite(), rechtecke.get(i).getHoehe());
		}
	}
}
