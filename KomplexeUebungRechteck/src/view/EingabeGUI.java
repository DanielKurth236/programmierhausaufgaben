package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import model.Rechteck;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class EingabeGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField_x;
	private JTextField textField_y;
	private JTextField textField_breite;
	private JTextField textField_hoehe;
	private JLabel JLabel_hoehe;
	private JLabel JLabel_breite;
	BunteRechteckeController brc = new BunteRechteckeController();


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EingabeGUI frame = new EingabeGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EingabeGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 250, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 36, 86, 0, 0};
		gbl_contentPane.rowHeights = new int[]{14, 20, 20, 20, 20, 23, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel JLabel_Headline = new JLabel("neues Rechteck");
		GridBagConstraints gbc_JLabel_Headline = new GridBagConstraints();
		gbc_JLabel_Headline.fill = GridBagConstraints.HORIZONTAL;
		gbc_JLabel_Headline.anchor = GridBagConstraints.NORTH;
		gbc_JLabel_Headline.insets = new Insets(0, 0, 5, 5);
		gbc_JLabel_Headline.gridx = 2;
		gbc_JLabel_Headline.gridy = 0;
		contentPane.add(JLabel_Headline, gbc_JLabel_Headline);
		
		JLabel JLabel_x = new JLabel("X");
		GridBagConstraints gbc_JLabel_x = new GridBagConstraints();
		gbc_JLabel_x.fill = GridBagConstraints.HORIZONTAL;
		gbc_JLabel_x.insets = new Insets(0, 0, 5, 5);
		gbc_JLabel_x.gridx = 1;
		gbc_JLabel_x.gridy = 1;
		contentPane.add(JLabel_x, gbc_JLabel_x);
		
		textField_x = new JTextField();
		textField_x.setColumns(10);
		GridBagConstraints gbc_textField_x = new GridBagConstraints();
		gbc_textField_x.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_x.anchor = GridBagConstraints.NORTH;
		gbc_textField_x.insets = new Insets(0, 0, 5, 5);
		gbc_textField_x.gridx = 2;
		gbc_textField_x.gridy = 1;
		contentPane.add(textField_x, gbc_textField_x);
		
		JLabel JLabel_y = new JLabel("Y");
		GridBagConstraints gbc_JLabel_y = new GridBagConstraints();
		gbc_JLabel_y.fill = GridBagConstraints.HORIZONTAL;
		gbc_JLabel_y.insets = new Insets(0, 0, 5, 5);
		gbc_JLabel_y.gridx = 1;
		gbc_JLabel_y.gridy = 2;
		contentPane.add(JLabel_y, gbc_JLabel_y);
		
		textField_y = new JTextField();
		textField_y.setColumns(10);
		GridBagConstraints gbc_textField_y = new GridBagConstraints();
		gbc_textField_y.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_y.anchor = GridBagConstraints.NORTH;
		gbc_textField_y.insets = new Insets(0, 0, 5, 5);
		gbc_textField_y.gridx = 2;
		gbc_textField_y.gridy = 2;
		contentPane.add(textField_y, gbc_textField_y);
		
		JLabel_breite = new JLabel("Breite");
		GridBagConstraints gbc_JLabel_breite = new GridBagConstraints();
		gbc_JLabel_breite.anchor = GridBagConstraints.WEST;
		gbc_JLabel_breite.insets = new Insets(0, 0, 5, 5);
		gbc_JLabel_breite.gridx = 1;
		gbc_JLabel_breite.gridy = 3;
		contentPane.add(JLabel_breite, gbc_JLabel_breite);
		
		textField_breite = new JTextField();
		textField_breite.setColumns(10);
		GridBagConstraints gbc_textField_breite = new GridBagConstraints();
		gbc_textField_breite.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_breite.anchor = GridBagConstraints.NORTH;
		gbc_textField_breite.insets = new Insets(0, 0, 5, 5);
		gbc_textField_breite.gridx = 2;
		gbc_textField_breite.gridy = 3;
		contentPane.add(textField_breite, gbc_textField_breite);
		
		JLabel_hoehe = new JLabel("H\u00F6he");
		GridBagConstraints gbc_JLabel_hoehe = new GridBagConstraints();
		gbc_JLabel_hoehe.fill = GridBagConstraints.HORIZONTAL;
		gbc_JLabel_hoehe.insets = new Insets(0, 0, 5, 5);
		gbc_JLabel_hoehe.gridx = 1;
		gbc_JLabel_hoehe.gridy = 4;
		contentPane.add(JLabel_hoehe, gbc_JLabel_hoehe);
		
		textField_hoehe = new JTextField();
		textField_hoehe.setColumns(10);
		GridBagConstraints gbc_textField_hoehe = new GridBagConstraints();
		gbc_textField_hoehe.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_hoehe.anchor = GridBagConstraints.NORTH;
		gbc_textField_hoehe.insets = new Insets(0, 0, 5, 5);
		gbc_textField_hoehe.gridx = 2;
		gbc_textField_hoehe.gridy = 4;
		contentPane.add(textField_hoehe, gbc_textField_hoehe);
		
		JButton Button_add = new JButton("zur Liste hinzuf\u00FCgen");
		Button_add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int x = Integer.parseInt(textField_x.getText());
				int y = Integer.parseInt(textField_y.getText());
				int breite = Integer.parseInt(textField_breite.getText());
				int hoehe = Integer.parseInt(textField_hoehe.getText());
				Rechteck r = new Rechteck(x,y,breite,hoehe);
				brc.add(r);
				System.out.println(brc.getRechtecke());
				System.out.println(brc.getRechtecke().size());
			}
		});
		
		GridBagConstraints gbc_Button_add = new GridBagConstraints();
		gbc_Button_add.fill = GridBagConstraints.HORIZONTAL;
		gbc_Button_add.insets = new Insets(0, 0, 0, 5);
		gbc_Button_add.anchor = GridBagConstraints.NORTH;
		gbc_Button_add.gridwidth = 2;
		gbc_Button_add.gridx = 1;
		gbc_Button_add.gridy = 5;
		contentPane.add(Button_add, gbc_Button_add);
	}
}
