package model;

import java.util.Random;

public class Rechteck {

	private int breite,hoehe;
	private Punkt p;
	static Random rechteck = new Random();

	public Rechteck() {
		this.p = new Punkt();
		this.breite = 0;
		this.hoehe = 0;
	}

	public Rechteck(int x, int y, int breite, int hoehe) {
		super();
		this.p = new Punkt(x,y);
		setBreite(breite);
		setHoehe(hoehe);
	}

	public int getX() {
		return this.p.getX();
	}

	public void setX(int x) {
		this.p.setX(x);
	}

	public int getY() {
		return this.p.getY();
	}

	public void setY(int y) {
		this.p.setY(y);
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		this.breite = Math.abs(breite);
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		this.hoehe = Math.abs(hoehe);
	}
	
	public boolean enthaelt(int x, int y) {
		return p.getX() <= x && x <= p.getX() + breite && p.getY() <= y && y <= p.getY() + hoehe;
	}
	
	public boolean enthaelt(Punkt p) {
		return enthaelt(p.getX(), p.getY());
	}
	
	public boolean enthaelt(Rechteck rechteck) {
		Punkt p1 = new Punkt(rechteck.getX(), rechteck.getY());
		Punkt p2 = new Punkt(rechteck.getX()+rechteck.getBreite(), rechteck.getY()+rechteck.getHoehe());
		
		return enthaelt(p1) && enthaelt(p2);
	}
	
	public static Rechteck generiereZufallsRechteck() {
		Rechteck r = new Rechteck();
		r.setX(rechteck.nextInt(1200 +1));
		r.setY(rechteck.nextInt(1000 +1));
		r.setBreite(rechteck.nextInt(1200 - r.getX() +1));
		r.setHoehe(rechteck.nextInt(1000 - r.getY() +1));	
		return r;
	}

	@Override
	public String toString() {
		return "Rechteck [breite=" + breite + ", hoehe=" + hoehe + ", p=" + p + "]";
	}

	
	
}
